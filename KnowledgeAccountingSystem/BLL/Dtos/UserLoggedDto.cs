﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Dtos
{
    public class UserLoggedDto
    {
        public long UserId { get; set; }

        public long? ProgrammerId { get; set; }
        public string Login { get; set; }
        public string Role { get; set; }
        public string Token { get; set; }

    }
}
