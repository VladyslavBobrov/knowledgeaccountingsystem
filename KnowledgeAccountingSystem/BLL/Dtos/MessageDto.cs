﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Dtos
{
    public class MessageDto
    {
        public long Id { get; set; }

        public long AreaId { get; set; }

        public long UserId { get; set; }

        public string Login { get; set; }

        public string Text { get; set; }

        public DateTime Date { get; set; }

    }
}
