﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Dtos
{
    public class ProgrammerDto
    {
        public long ProgrammerId { get; set; }
        public long? UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public DateTime DOJ { get; set; }
        public DateTime CreatedDate { get; set; }
        public string PhotoFileName { get; set; }
    }
}
