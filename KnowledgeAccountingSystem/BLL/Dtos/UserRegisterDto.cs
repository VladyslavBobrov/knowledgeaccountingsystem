﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Dtos
{
    public class UserRegisterDto
    {
        public string Login { get; set; }
        public string Email { get; set; }

        public string Password { get; set; }
        public long ProgrammerId { get; set; }

    }
}
