﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Dtos
{
    public class AddMessageDto
    {

        public long UserId { get; set; }

        public long AreaId { get; set; }

        public string Text { get; set; }

        public DateTime Date { get; set; }
    }

}
