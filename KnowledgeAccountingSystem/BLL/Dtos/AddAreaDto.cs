﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Dtos
{
    public class AddAreaDto
    {
        public string Name { get; set; }

        public string Description { get; set; }

    }
}
