﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Dtos
{
    public class ProgAreaDto
    {
        public long Id { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public bool IsSubscribed { get; set; }
       
    }
}
