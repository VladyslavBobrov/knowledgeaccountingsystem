﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Dtos
{
    public class AddDeleteProgrammerAreaDto
    {
        public long ProgrammerId { get; set; }

        public long AreaId { get; set; }

    }
}
