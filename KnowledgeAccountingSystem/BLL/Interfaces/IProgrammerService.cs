﻿using System;
using System.Collections.Generic;
using System.Text;
using BLL.Dtos;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface IProgrammerService
    {
        Task<List<ProgrammerDto>> GetAllProgrammersAsync();

        Task<List<ProgrammerDto>> GetProgrammersByRole(string name);
        Task<List<ProgrammerDto>> GetProgrammersById(int id);
        Task UpdateProgrammer(ProgrammerDto programmerDto);
        Task AddProgrammer(ProgrammerDto programmerDto);
        Task DeleteProgrammer(long programmerId);
    }
}
