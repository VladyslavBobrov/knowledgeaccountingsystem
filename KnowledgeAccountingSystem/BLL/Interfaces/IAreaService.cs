﻿using System;
using System.Collections.Generic;
using System.Text;
using BLL.Dtos;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface IAreaService
    {
        Task AddAreaAsync(AddAreaDto addAreaDto);

        Task AddProgrammerAreaAsync(AddDeleteProgrammerAreaDto addProgrammerAreaDto);
        Task DeleteProgrammerAreaAsync(AddDeleteProgrammerAreaDto deleteProgrammerAreaDto);

        Task<List<AreaDto>> GetAllAreasAsync();

        Task DeleteAreaAsync(long areaId);

        Task<List<ProgAreaDto>> GetProgrammerAreasAsync(long userId);
        Task<List<ProgAreaDto>> GetAreasByProgrammerIdAsync(long userId);
        Task<AreaDto> GetArea(long areaId);

    }
}
