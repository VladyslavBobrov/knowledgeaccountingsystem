﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BLL.Dtos;

namespace BLL.Interfaces
{
    public interface IAuthenticationService
    {
        Task RegisterAsync(UserRegisterDto userRegister);

        Task<UserLoggedDto> Authenticate(UserAuthDto userAuthDto);

        Task<long> GetProgrammerId(ProgrammerDto progCheckDto);

    }

}
