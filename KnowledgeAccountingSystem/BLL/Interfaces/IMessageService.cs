﻿using System;
using System.Collections.Generic;
using System.Text;
using BLL.Dtos;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface IMessageService
    {
        Task AddMessage(AddMessageDto addMessageDto);

        Task<List<MessageDto>> GetMessages(long areaId);

    }
}
