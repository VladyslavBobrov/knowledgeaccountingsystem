﻿using System;
using System.Collections.Generic;
using System.Text;
using BLL.Dtos;

using DAL.Entities;

namespace BLL.Mappers
{

    public class ProgrammerMapper : IMapper<Programmer, ProgrammerDto>
    {
        public Programmer Map(ProgrammerDto item)
        {
            return new Programmer
            {
                FirstName = item.FirstName,
                LastName = item.LastName,
                Gender = item.Gender,
                DOJ = item.DOJ,
                UserId = item.UserId,
                CreatedDate = item.CreatedDate,
                PhotoFileName = item.PhotoFileName,
                ProgrammerId = item.ProgrammerId
            };
        }

        public ProgrammerDto Map(Programmer item)
        {
            return new ProgrammerDto {
                FirstName = item.FirstName,
                LastName = item.LastName,
                Gender = item.Gender,
                DOJ = item.DOJ,
                UserId = item.UserId,
                CreatedDate = item.CreatedDate,
                PhotoFileName = item.PhotoFileName,
                ProgrammerId = item.ProgrammerId
            };
        }

        public List<Programmer> MapList(List<ProgrammerDto> dtos)
        {
            var programmers = new List<Programmer>();
            foreach (var dto in dtos)
            {
                programmers.Add(Map(dto));
            }
            return programmers;
        }

        public List<ProgrammerDto> MapList(List<Programmer> entities)
        {
            var programmers = new List<ProgrammerDto>();
            foreach (var entity in entities)
            {
                programmers.Add(Map(entity));
            }
            return programmers;
        }
    }

}
