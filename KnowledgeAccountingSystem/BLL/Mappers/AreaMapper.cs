﻿using System;
using System.Collections.Generic;
using System.Text;
using BLL.Dtos;
using DAL.Entities;

namespace BLL.Mappers
{
    public class AreaMapper : IMapper<ProgrammerArea, AddDeleteProgrammerAreaDto>
    {
   
        public AddDeleteProgrammerAreaDto Map(ProgrammerArea item)
        {
            return new AddDeleteProgrammerAreaDto
            {
                ProgrammerId = item.ProgrammerId,
                AreaId = item.AreaId,
            };
        }
        public ProgrammerArea Map(AddDeleteProgrammerAreaDto item)
        {
            return new ProgrammerArea
            {
                ProgrammerId = item.ProgrammerId,
                AreaId = item.AreaId,
            };
        }


        public List<AddDeleteProgrammerAreaDto> MapList(List<ProgrammerArea> dtos)
        {
            var programmersAreas = new List<AddDeleteProgrammerAreaDto>();
            foreach (var dto in dtos)
            {
                programmersAreas.Add(Map(dto));
            }
            return programmersAreas;
        }
        public List<ProgrammerArea> MapList(List<AddDeleteProgrammerAreaDto> dtos)
        {
            var programmersAreas = new List<ProgrammerArea>();
            foreach (var dto in dtos)
            {
                programmersAreas.Add(Map(dto));
            }
            return programmersAreas;
        }

    }
}
