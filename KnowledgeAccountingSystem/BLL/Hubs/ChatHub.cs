﻿using BLL.Dtos;
using System.Threading.Tasks;
using System.Text;
using Microsoft.AspNetCore.SignalR;

namespace BLL.Hubs
{
    public class ChatHub : Hub
    {
        public async Task Send(MessageDto message)
        {
            await Clients.All.SendAsync("Send message", message);
        }

        public override Task OnConnectedAsync()
        {

            return base.OnConnectedAsync();
        }
    }

}
