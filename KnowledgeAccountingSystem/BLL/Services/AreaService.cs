﻿using System;
using System.Collections.Generic;
using System.Text;
using DAL.Entities;
using DAL.Repositories;
using System.Threading.Tasks;
using BLL.Dtos;
using BLL.Interfaces;
using BLL.Mappers;

namespace BLL.Services
{
    public class AreaService : IAreaService
    {
        IRepository<Programmer> _programmerRepository;
        IRepository<Area> _areaRepository;
        IRepository<ProgrammerArea> _programmerAreaRepository;
        
        public AreaService(IRepository<Programmer> programmerRepository,
            IRepository<Area> areaRepository,
            IRepository<ProgrammerArea> programmerAreaRepository)
        {
            _programmerAreaRepository = programmerAreaRepository;
            _programmerRepository = programmerRepository;
            _areaRepository = areaRepository;
     
        }
        public async Task AddAreaAsync(AddAreaDto addAreaDto)
        {
            await _areaRepository.AddAsync(new Area { Name = addAreaDto.Name, Description = addAreaDto.Description });
        }

        public async Task AddProgrammerAreaAsync(AddDeleteProgrammerAreaDto addProgrammerAreaDto)
        {
            await _programmerAreaRepository.AddAsync(new ProgrammerArea { AreaId=addProgrammerAreaDto.AreaId,ProgrammerId=addProgrammerAreaDto.ProgrammerId});
        }

        public async Task DeleteAreaAsync(long areaId)
        {
            await _areaRepository.DeleteAsync(c => c.Id == areaId);
        }

        public async Task DeleteProgrammerAreaAsync(AddDeleteProgrammerAreaDto addProgrammerAreaDto)
        {
      
            await _programmerAreaRepository.DeleteAsync(sc => sc.AreaId == addProgrammerAreaDto.AreaId && sc.ProgrammerId == addProgrammerAreaDto.ProgrammerId);
        }

        public async Task<List<AreaDto>> GetAllAreasAsync()
        {
            var areas = await _areaRepository.GetAllAsync();
            var areasDto = new List<AreaDto>();
            foreach (var area in areas)
            {
                areasDto.Add(new AreaDto
                {
                    Id = area.Id,
                    Description = area.Description,
                    Name = area.Name
                });
            }

            return areasDto;
        }

        public async Task<AreaDto> GetArea(long areaId)
        {

            var area = await _areaRepository.GetByAsync(c => c.Id == areaId);
            return new AreaDto { Id = areaId, Description = area.Description, Name = area.Name };
        }

        public async Task<List<ProgAreaDto>> GetProgrammerAreasAsync(long programmerId)
        {
            var areasDtos = new List<ProgAreaDto>();
            var areas = await _areaRepository.GetAllAsync();
            foreach (var area in areas)
            {
                var programmerArea = await _programmerAreaRepository.GetByAsync(sc => sc.AreaId == area.Id && sc.ProgrammerId == programmerId);
                {
                    areasDtos.Add(new ProgAreaDto
                    {
                        Id = area.Id,
                        Description = area.Description,
                        Name = area.Name,
                        IsSubscribed = programmerArea != null
                    });
                }
            }


            return areasDtos;
        }
        public async Task<List<ProgAreaDto>> GetAreasByProgrammerIdAsync(long programmerId)
        {
            var areasDtos = new List<ProgAreaDto>();
            var areas = await _areaRepository.GetAllAsync();
            foreach (var area in areas)
            {
                var programmerArea = await _programmerAreaRepository.GetByAsync(sc => sc.AreaId == area.Id && sc.ProgrammerId == programmerId);
                if (programmerArea != null)
                {
                    areasDtos.Add(new ProgAreaDto
                    {
                        Id = programmerArea.ProgrammerAreaId,
                        Description = area.Description,
                        Name = area.Name,
                        IsSubscribed = programmerArea != null
                    });
                }
            }


            return areasDtos;
        }


    }

}
