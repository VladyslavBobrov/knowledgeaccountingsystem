﻿using System;
using System.Collections.Generic;
using System.Text;
using DAL.Entities;
using DAL.Repositories;
using System.Threading.Tasks;
using BLL.Dtos;
using BLL.Interfaces;
using BLL.Mappers;

namespace BLL.Services
{
    public class ProgrammerService : IProgrammerService
    {
        IRepository<Programmer> _programmerRepository;
        IMapper<Programmer, ProgrammerDto> _programmerMapper;
        public ProgrammerService(IRepository<Programmer> programmerRepository, IMapper<Programmer, ProgrammerDto> programmerMapper)
        {
            _programmerRepository = programmerRepository;
            _programmerMapper = programmerMapper;
        }
        public async Task AddProgrammer(ProgrammerDto programmerDto)
        {
            await _programmerRepository.AddAsync(_programmerMapper.Map(programmerDto));
        }

        public async Task DeleteProgrammer(long programmerId)
        {
            await _programmerRepository.DeleteAsync(s => s.ProgrammerId == programmerId);
        }

        public async Task<List<ProgrammerDto>> GetAllProgrammersAsync()
        {
            return  _programmerMapper.MapList(await _programmerRepository.GetAllAsync());
        }


        public async Task<List<ProgrammerDto>> GetProgrammersById(int id)
        {
            return _programmerMapper.MapList(await _programmerRepository.GetAllByAsync(s => s.ProgrammerId == id));

        }

        public async Task<List<ProgrammerDto>> GetProgrammersByRole(string role)
        {
            return _programmerMapper.MapList(await _programmerRepository.GetAllByAsync(s => s.User.Role.Contains(role)));
        }

        public async Task UpdateProgrammer(ProgrammerDto programmerDto)
        {
            await _programmerRepository.Update(_programmerMapper.Map(programmerDto));
        }

    }

}
