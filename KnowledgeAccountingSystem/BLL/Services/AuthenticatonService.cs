﻿using BLL.Dtos;
using BLL.Helpers;
using BLL.Interfaces;
using DAL.Entities;
using DAL.Repositories;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class AuthenticatonService : IAuthenticationService
    {

        IRepository<User> _userRepository;
        IRepository<Programmer> _programmerRepository;
        private readonly AppSettings _appSettings;
        public AuthenticatonService(IRepository<User> userRepository, IRepository<Programmer> programmerRepository, IOptions<AppSettings> appSettings)
        {
            _userRepository = userRepository;
            _programmerRepository = programmerRepository;
            _appSettings = appSettings.Value;
        }

        public async Task<UserLoggedDto> Authenticate(UserAuthDto userAuthDto)
        {
            var user = await _userRepository.GetByAsync(u => u.Login == userAuthDto.Login && u.Password == userAuthDto.Password && u.Email == userAuthDto.Email);
            if (user == null)
                throw new Exception("Login or password are invalid. Try again");

            // authentication successful so generate jwt token
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Id.ToString()),
                    new Claim(ClaimTypes.Role, user.Role)
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            var programmer = await _programmerRepository.GetByAsync(s => s.UserId == user.Id);

            UserLoggedDto userLoggedDto = new UserLoggedDto
            {
                UserId = user.Id,
                Login = user.Login,
                Role = user.Role,
                Token = tokenHandler.WriteToken(token),
                ProgrammerId = programmer?.ProgrammerId
            };

            return userLoggedDto;

        }

        public async Task RegisterAsync(UserRegisterDto userRegister)
        {
            var programmer = await _programmerRepository.GetByAsync(st => st.ProgrammerId == userRegister.ProgrammerId);
            if (programmer == null)
            {
                throw new Exception("There is no such programmer in database");
            }
            if (programmer.UserId != null)
            {
                throw new Exception("This programmer has user");
            }

            var user = await _userRepository.GetByAsync(u => u.Login == userRegister.Login);
            if (user != null)
            {
                throw new Exception("User " + user.Login + " exist in database");
            }

            await _userRepository.AddAsync(new User
            {
                Login = userRegister.Login,
                Password = userRegister.Password,
                Email = userRegister.Email,
                Role = Role.Programmer
            });
            var createdUser = await _userRepository.GetByAsync(u => u.Login == userRegister.Login);
            programmer.UserId = createdUser.Id;
            await _programmerRepository.Update(programmer);
        }

        public async Task<long> GetProgrammerId(ProgrammerDto progCheckDto)
        {
            var programmer = await _programmerRepository.GetByAsync(pr => pr.FirstName == progCheckDto.FirstName);
            if (programmer == null)
            {
                throw new Exception("There is no user with this name surname");
            }
            return programmer.ProgrammerId;
        }
    }

}
