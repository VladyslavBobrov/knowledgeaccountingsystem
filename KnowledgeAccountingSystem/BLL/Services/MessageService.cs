﻿using System;
using System.Collections.Generic;
using System.Text;
using DAL.Entities;
using DAL.Repositories;
using System.Threading.Tasks;
using BLL.Dtos;
using BLL.Hubs;
using BLL.Interfaces;
using Microsoft.AspNetCore.SignalR;

namespace BLL.Services
{
    public class MessageService : IMessageService
    {
        IRepository<Message> _messageRepository;
        IRepository<User> _userRepository;
        IHubContext<ChatHub> _chartHub;
        public MessageService(IRepository<Message> messageRepository, IRepository<User> userRepository, IHubContext<ChatHub> chartHub)
        {
            _messageRepository = messageRepository;
            _userRepository = userRepository;
            _chartHub = chartHub;
        }

        public async Task AddMessage(AddMessageDto addMessageDto)
        {
            await _messageRepository.AddAsync(new Message
            {
                AreaId = addMessageDto.AreaId,
                Date = addMessageDto.Date,
                Text = addMessageDto.Text,
                UserId = addMessageDto.UserId
            });

            await _chartHub.Clients.All.SendAsync("getNewMessage", new MessageDto
            {
                Login = (await _userRepository.GetByAsync(u => u.Id == addMessageDto.UserId)).Login,
                AreaId = addMessageDto.AreaId,
                Date = addMessageDto.Date,
                Text = addMessageDto.Text,
                UserId = addMessageDto.UserId
            });
        }

        public async Task<List<MessageDto>> GetMessages(long areaId)
        {
            var messages = await _messageRepository.GetAllByAsync(m => m.AreaId == areaId);
            messages.Reverse();
            var messageDtos = new List<MessageDto>();
            foreach (var message in messages)
            {
                messageDtos.Add(new MessageDto
                {
                    Id = message.Id,
                    Date = message.Date,
                    AreaId = message.AreaId,
                    Login = (await _userRepository.GetByAsync(u => u.Id == message.UserId)).Login,
                    Text = message.Text,
                    UserId = message.UserId
                });
            }

            return messageDtos;
        }
    }

}
