﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.Data;
using System.Data.SqlClient;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using BLL.Interfaces;
using BLL.Dtos;
using Microsoft.AspNetCore.Authorization;
using DAL.Entities;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProgrammerController : ControllerBase
    {
        private readonly IProgrammerService _programmerService;
        private readonly IWebHostEnvironment _env;
        public ProgrammerController(IProgrammerService programmerService,IWebHostEnvironment env)
        {
            _programmerService = programmerService;
            _env = env;
        }


        //GET : api/<ProgrammerController>
        [Authorize(Roles = Role.Admin)]
        [HttpGet]
        public async Task<IEnumerable<ProgrammerDto>> Get()
        {
            var prog = await _programmerService.GetAllProgrammersAsync();
            return await _programmerService.GetAllProgrammersAsync();
        }

        //GET : api/<ProgrammerController>
        [Authorize(Roles = Role.Manager)]
        [HttpGet("ProgrammerListForManager")]
        public async Task<IEnumerable<ProgrammerDto>> ProgrammerListForManager()
        {
            return await _programmerService.GetProgrammersByRole(Role.Programmer);
        }

        //GET : api/<ProgrammerController>
        [Authorize(Roles = Role.Programmer)]
        [HttpGet("{id}")]
        public async Task<IEnumerable<ProgrammerDto>> Get(int id)
        {
            try
            {
                return await _programmerService.GetProgrammersById(id);
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }

        }
        ///POST api/<ProgrammerController>/5
        [HttpPost]
        public async Task AddProgrammer([FromBody] ProgrammerDto programmer)
        {
            try
            {
                await _programmerService.AddProgrammer(programmer);
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }

        // PUT api/<ProgrammerController>/5
        [HttpPut]
        public async Task UpdateProgrammer(ProgrammerDto programmer)
        {
            try
            {
                await _programmerService.UpdateProgrammer(programmer);
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }

        // DELETE api/<ProgrammerController>/5
        [Authorize(Roles = Role.Admin)]
        [HttpDelete("{programmerId}")]
        public async Task Delete(long programmerId)
        {
            try
            {
                await _programmerService.DeleteProgrammer(programmerId);
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }

        [Route("SaveFile")]
        [HttpPost]
        public JsonResult SaveFile()
        {
            try
            {
                var httpRequest = Request.Form;
                var postedFile = httpRequest.Files[0];
                string filename = postedFile.FileName;
                var physicalPath = _env.ContentRootPath + "/Photos/" + filename;
                using (var stream = new FileStream(physicalPath,FileMode.Create))
                {
                    postedFile.CopyTo(stream);
                }
                return new JsonResult(filename);
            }
            catch (Exception)
            {
                return new JsonResult("anonymus.png");
            }
        }
    }
}
