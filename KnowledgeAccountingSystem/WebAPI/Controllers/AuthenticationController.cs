﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.Dtos;
using BLL.Services;
using BLL.Interfaces;
using Microsoft.AspNetCore.Authorization;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        IAuthenticationService _authenticationService;
        public AuthenticationController(IAuthenticationService authenticationService)
        {
            _authenticationService = authenticationService;
        }
        // GET: api/<AuthenticationController>
        [HttpGet("GetProgrammerId")]
        [AllowAnonymous]
        public async Task<long> GetSProgrammerId([FromQuery] ProgrammerDto studCheck)
        {
            try
            {
                return await _authenticationService.GetProgrammerId(studCheck);
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }


        // GET api/<AuthenticationController>/5
        [AllowAnonymous]
        [HttpPost("register")]
        public async Task Register([FromBody] UserRegisterDto userRegister)
        {
            try
            {
                await _authenticationService.RegisterAsync(userRegister);
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }

        //// POST api/<AuthenticationController>
        [AllowAnonymous]
        [HttpPost]
        public async Task<UserLoggedDto> Login(UserAuthDto userAuthDto)
        {
            try
            {
                return await _authenticationService.Authenticate(userAuthDto);
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }

    }
}
