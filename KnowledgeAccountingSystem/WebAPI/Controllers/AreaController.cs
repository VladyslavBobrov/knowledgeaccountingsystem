﻿using BLL.Dtos;
using BLL.Interfaces;
using DAL.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AreaController : ControllerBase
    {
        IAreaService _areaService;
        public AreaController(IAreaService areaService)
        {
            _areaService = areaService;
        }


        //GET : api/<AreaController>
        [Authorize(Roles = Role.Admin)]
        [HttpGet]
        public async Task<List<AreaDto>> GetAreasAsync()
        {
            try
            {
                return await _areaService.GetAllAreasAsync();
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }


        //GET : api/<AreaController>
        [HttpGet("getProgrammerAreas")]
        public async Task<List<ProgAreaDto>> GetAreaByProgIdAsync([FromQuery] long programmerId)
        {
            try
            {
                return await _areaService.GetProgrammerAreasAsync(programmerId);
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }
        [HttpGet("getAreasByProgrammerId")]
        public async Task<List<ProgAreaDto>> GetAreasByProgrammerId([FromQuery] long programmerId)
        {
            try
            {
                return await _areaService.GetAreasByProgrammerIdAsync(programmerId);
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }
        //POST api/<AreaController>/5
        [Authorize(Roles = Role.Admin)]
        [HttpPost]
        public async Task AddAreaAsync([FromBody] AddAreaDto addAreaDto)
        {
            try
            {
                await _areaService.AddAreaAsync(addAreaDto);
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }
        //POST api/<ProgrammerController>/5
        //[Authorize(Roles = Role.Programmer)]
        [HttpPost("addProgrammerArea")]
        public async Task AddProgrammerAreaAsync([FromBody] AddDeleteProgrammerAreaDto studentAreaDto)
        {
            try
            {
                await _areaService.AddProgrammerAreaAsync(studentAreaDto);
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }
        //Delete api/<AreaController>/5
        [Authorize(Roles = Role.Programmer)]
        [HttpDelete("deleteProgrammerArea")]
        public async Task DeleteProgrammerAreaAsync([FromQuery] AddDeleteProgrammerAreaDto studentAreaDto)
        {
            try
            {
                await _areaService.DeleteProgrammerAreaAsync(studentAreaDto);
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }
        //Delete api/<AreaController>/5
        [Authorize(Roles = Role.Admin)]
        [HttpDelete]
        public async Task DeleteAreaAsync([FromQuery] long areaId)
        {
            try
            {
                await _areaService.DeleteAreaAsync(areaId);
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }

        //GET api/<AreaController>/5
        [Authorize]
        [HttpGet("getArea")]
        public async Task<AreaDto> GetArea([FromQuery] long areaId)
        {
            try
            {
                return await _areaService.GetArea(areaId);
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message);
            }
        }


    }

}
