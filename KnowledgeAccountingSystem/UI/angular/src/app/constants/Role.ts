export class Role {
    static readonly Admin = 'Admin';
    static readonly Programmer = 'Programmer';
    static readonly Manager = 'Manager';
  }
  