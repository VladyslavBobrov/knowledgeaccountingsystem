import { AuthService } from './services/auth.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
 public title = "Programmers-App";
 public login!: string;
 isAuth = false;
 isAdmin = false;
 isManager = false;
 isProgrammer = false;

 constructor(private authService: AuthService, private router: Router){
   this.authService.currentUser.subscribe( user => {
     if( user){
       this.isAuth = true;
       this.login = user.login;
     }
     else{
       this.isAdmin = false;
       this.isAuth = false;
       this.isManager = false;
       this.isProgrammer = false;
       this.login = '';
     }
   })

 }
 ngOnInit(){
  let currentUser =  this.authService.getCurrentUserValue();
  this.isAuth = currentUser == null ? false : true;
  this.login = currentUser == null ? '' : currentUser.login;
  this.isAdmin = currentUser.role != 'Admin'? false : true;
  this.isManager = currentUser.role != 'Manager'? false : true;
  this.isProgrammer = currentUser.role != 'Programmer'? false : true;
 }
 logout(){

   this.authService.logout();
   this.router.navigate(['/login']);
 }
}
