import { AdminAreaComponent } from './components/admin-area/admin-area.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { ProgrammersComponent } from './components/programmers/programmers.component';
import { NgModule } from '@angular/core';
import { ExtraOptions,RouterModule, Routes } from '@angular/router';
import { ProgrammerInformationComponent } from './components/programmer-information/programmer-information.component';
import { AuthGuard } from './helpers/auth-guard';
import { ProgrammerAreaComponent } from './components/programmer-area/programmer-area.component';
import { AreaComponent } from './components/area/area.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { userInfo } from 'os';
import {AuthService} from './services/auth.service';
import { ManagerComponent } from './components/manager/manager.component';
const routes: Routes = [
  { path: 'register', component: RegisterComponent },
  { path: 'login', component: LoginComponent , canActivate:[AuthService]},
  { path: 'area/:id', component: AreaComponent, canActivate: [AuthGuard] , data: {
                    roles: ['Admin', 'Programmer']
                  }
                },
  { path: 'programmers', component: ProgrammersComponent , canActivate: [AuthGuard],
                  data: {
                    roles: ['Admin','Manager']
                  }
                },
 { path: 'manager', component: ManagerComponent , canActivate: [AuthGuard],
                data: {
                  roles: ['Manager']
                }
              },              
  { path: 'AreaManage', component: AdminAreaComponent , canActivate: [AuthGuard],
                data: {
                  roles: ['Admin']
                }
              },
  { path: 'programmer-information', component: ProgrammerInformationComponent, canActivate: [AuthGuard],
  data: {
    roles: ['Programmer','Admin']
  }},
  { path: 'programmer-area', component: ProgrammerAreaComponent, canActivate: [AuthGuard],
  data: {
    roles: ['Programmer']
  }},


  { path: '', redirectTo: '', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent },
];
const config: ExtraOptions = {
  useHash: false,
};
@NgModule({

  imports: [RouterModule.forRoot(routes,config),  ReactiveFormsModule,   FormsModule,CommonModule],
  exports: [RouterModule]
})
export class AppRoutingModule { }
