import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgrammerInformationComponent } from './programmer-information.component';

describe('ProgrammerInformationComponent', () => {
  let component: ProgrammerInformationComponent;
  let fixture: ComponentFixture<ProgrammerInformationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProgrammerInformationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgrammerInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
