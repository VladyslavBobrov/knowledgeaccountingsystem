import { Component, OnInit } from '@angular/core';
import { FormBuilder, ReactiveFormsModule, FormControl, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import {formatDate} from '@angular/common';
import { Programmer } from '../../interfaces/Programmer';
import { ProgrammerService } from 'src/app/services/programmer.service';
import { User } from 'src/app/interfaces/user';
import { AuthService } from './../../services/auth.service';
import { first } from 'rxjs/operators';
@Component({
  selector: 'app-programmer-information',
  templateUrl: './programmer-information.component.html',
  styleUrls: ['./programmer-information.component.css']
})
export class ProgrammerInformationComponent implements OnInit {

  constructor(private ProgrammerService: ProgrammerService,private authService: AuthService, private formBuilder: FormBuilder,private router: Router) { }

  
 
  genderList = ["Male","Female"];
  user!: User;
  prog!:Programmer[];
  PhotoFileName:string="anonymus.png";
  PhotoFilePath=this.ProgrammerService.PhotoUrl + this.PhotoFileName;
  addProgrammerForm = this.formBuilder.group({
    firstName:  [ '', [Validators.required, Validators.minLength(1) ]],
    lastName: ['', [Validators.required, Validators.minLength(1)]] ,
    gender: [ '', [Validators.required, Validators.minLength(1)]] ,
    doj : ['', [Validators.required ]],
    PhotoFileName: [this.PhotoFileName]
    });
  
  ngOnInit(): void {
    this.ProgrammerService.getProgrammerId(this.authService.getCurrentUserValue().programmerId)
   .subscribe((data) => {
     this.prog = data;
     console.log(data);
   });


 }

  closeClick(){
   this.ProgrammerService.getProgrammerId(this.authService.getCurrentUserValue().programmerId)
    .subscribe((data) => {
      this.prog = data;
      console.log(data);
    });
  }

  updateProgrammer(prog : Programmer){
    prog.programmerId = this.authService.getCurrentUserValue().programmerId;
    prog.userId = this.authService.getCurrentUserValue().userId;
    this.ProgrammerService.updateProgrammer(prog)
    .subscribe((data) => {
      console.log(data);
    });
  }

  uploadPhoto(event:any){
    var file=event.target.files[0];
    const formData:FormData=new FormData();
    formData.append('uploadedFile',file,file.name);

    this.ProgrammerService.UploadPhoto(formData).subscribe((data:any)=>{
      this.PhotoFileName=data.toString();
      this.PhotoFilePath=this.ProgrammerService.PhotoUrl+this.PhotoFileName;
    })
  }
  showArea(){
        this.router.navigate(['/programmer-area']);
    }

}
