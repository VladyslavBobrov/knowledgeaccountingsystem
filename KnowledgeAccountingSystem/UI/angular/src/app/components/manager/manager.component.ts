import { Component, OnInit } from '@angular/core';
import { Programmer } from '../../interfaces/Programmer';
import { ProgrammerService } from 'src/app/services/programmer.service';
import { FormBuilder, ReactiveFormsModule, FormControl, Validators, FormGroup } from '@angular/forms';
import { Area } from 'src/app/interfaces/area';
import { AddProgrammerArea } from 'src/app/interfaces/add-prog-area';
import { AreaService } from 'src/app/services/area.service';
import { ProgrammerArea } from 'src/app/interfaces/prog-area';

@Component({
  selector: 'app-manager',
  templateUrl: './manager.component.html',
  styleUrls: ['./manager.component.css']
})
export class ManagerComponent implements OnInit {
  Programmers: Programmer[] = [];
  Programmer!: Programmer;
  ProgrammerAreas!:ProgrammerArea[];
  ProgrammerAreasWithout!:ProgrammerArea[];
  ProgrammerListWithoutFilter:Programmer[] = [];
  ProgrammerAreaFilter:string="";
  constructor(private ProgrammerService: ProgrammerService,private areaService: AreaService, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.ProgrammerService.getProgrammerListForManager().subscribe((data) => {
      this.Programmers = data;
      this.ProgrammerListWithoutFilter=data;
      console.log(data);
  });
   
  }
  
  getAreasInfo(programmerId: number){
    this.areaService.getAreasByProgrammerId(programmerId).subscribe((data) => {
      this.ProgrammerAreas = data;
      this.ProgrammerAreasWithout=data;
      console.log(data);
  });
  }
  AreaFilter(){
    var ProgrammerAreaFilter = this.ProgrammerAreaFilter;

    this.ProgrammerAreas = this.ProgrammerAreasWithout.filter(function(el:any){
      return el.name.toString().toLowerCase().includes(
        ProgrammerAreaFilter.toString().trim().toLowerCase()
      );
    });
  }
  sortResult(prop:any,asc:any){
    this.Programmers = this.ProgrammerListWithoutFilter.sort(function(a:any,b:any){
      if(asc){
        return (a[prop]>b[prop]?1 :((a[prop]<b[prop]) ?-1 :0));
      }else{
        return (b[prop]>a[prop]?1 :((b[prop]<a[prop]) ?-1 :0));
      }
    });
  }

}
