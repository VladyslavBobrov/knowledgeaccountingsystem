
import { Component, OnInit } from '@angular/core';
import { ProgrammerArea } from 'src/app/interfaces/prog-area';
import { AuthService } from './../../services/auth.service';
import { AddProgrammerArea } from './../../interfaces/add-prog-area';
import { AreaService } from 'src/app/services/area.service';

@Component({
  selector: 'app-programmer-area',
  templateUrl: './programmer-area.component.html',
  styleUrls: ['./programmer-area.component.css']
})
export class ProgrammerAreaComponent implements OnInit {

  iPage  = 4;
  curPage = 1;
  count = 0;

  iPageUnsub  = 4;
  curPageUnsub = 1;
  countUnsub = 0;
  programmerId!: number;
  areas!: ProgrammerArea[];
  constructor(private areaService: AreaService, private authService: AuthService) {

  }

  ngOnInit(): void {
    this.programmerId = this.authService.getCurrentUserValue().programmerId;
    this.getProgrammerAreas();

  }

  getProgrammerAreas(): void{
    this.areaService.getProgrammerAreas(this.programmerId).subscribe(
      (areas) => {
        this.areas = areas;
      }
    );

  }

  onDataChange(event: any): void{
  this.curPage = event;
  this.getProgrammerAreas();
  }

  
  addProgrammerArea(area: ProgrammerArea): void{
    this.areaService.addProgrammerArea({areaId: area.id, programmerId: this.programmerId}).subscribe(
      () => {
        this.getProgrammerAreas();
      }
    );
  }

  deleteProgrammerArea(area: ProgrammerArea): void{
    this.areaService.deleteProgrammerArea({id: area.id, programmerId: this.programmerId}).subscribe(
      () => {
        this.getProgrammerAreas();
        console.log('area for programmer is deleted');
      }
    );
  }


}
