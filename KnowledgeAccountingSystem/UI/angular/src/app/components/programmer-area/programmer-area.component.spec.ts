/* tslint:disable:no-unused-variable */
import { async ,ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { ProgrammerAreaComponent } from './programmer-area.component';

describe('ProgrammerAreaComponent', () => {
  let component: ProgrammerAreaComponent;
  let fixture: ComponentFixture<ProgrammerAreaComponent>;

  beforeEach(async () => {
    TestBed.configureTestingModule({
      declarations: [ ProgrammerAreaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgrammerAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
