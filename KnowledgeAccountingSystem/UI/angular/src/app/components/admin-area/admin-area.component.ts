import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Area } from 'src/app/interfaces/area';
import { AreaService } from 'src/app/services/area.service';

@Component({
  selector: 'app-admin-area',
  templateUrl: './admin-area.component.html',
  styleUrls: ['./admin-area.component.css']
})
export class AdminAreaComponent implements OnInit {

  addAreaForm!: FormGroup;
  areas: Area[] = [];

  constructor(private areaService: AreaService) { }

  ngOnInit(): void {
    this.addAreaForm = new FormGroup({
      name: new FormControl(''),
      description: new FormControl('')
    });
    this.getAreas();

  }

  getAreas(): void{
    this.areaService.getAllAreas().subscribe(
        (areas: Area[]) => {
            this.areas = areas;
        }
    );
  }

  addArea(): void{
    this.areaService.addArea(this.addAreaForm.value).subscribe(
      () => {
        console.log('Area added');
        this.getAreas();
      }
    );
  }

  deleteArea(areaId: number): void{
        this.areaService.deleteArea(areaId).subscribe(() => {
          console.log('course deleted');
          this.getAreas();
        });
  }

}
