import { Programmer } from './../../interfaces/Programmer';
import { FormControl, FormGroup,Validators } from '@angular/forms';
import { AuthService } from './../../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { Role } from 'src/app/constants/Role';
import { Router } from '@angular/router';
import { User } from 'src/app/interfaces/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  form!: FormGroup;

  constructor(private authService: AuthService,private router: Router) { }

  
  ngOnInit() {
    this.form = new FormGroup({
      login : new FormControl('',Validators.required),
      password :new FormControl('',Validators.required),
      email! :new FormControl('',[Validators.required, Validators.email])
    })
  }
  login(){
    this.authService.login(this.form.value).subscribe(
      (user: User) => {

        if (user.role === Role.Admin){
            this.router.navigate(['/programmers']);
        }
        if (user.role === Role.Programmer){
          this.router.navigate(['/programmer-information']);
        }
        if (user.role === Role.Manager){
          this.router.navigate(['/manager']);
        }
      },
      (exc)=>{
        alert("Doesn't exist user with this login or password. Try again");
      }
    );
    }

}
