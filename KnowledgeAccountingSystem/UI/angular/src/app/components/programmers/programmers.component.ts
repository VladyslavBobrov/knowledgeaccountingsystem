import { Programmer } from '../../interfaces/Programmer';
import { ProgrammerService } from 'src/app/services/programmer.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, ReactiveFormsModule, FormControl, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import {formatDate} from '@angular/common';

@Component({
  selector: 'app-programmers',
  templateUrl: './programmers.component.html',
  styleUrls: ['./programmers.component.css']
})
export class ProgrammersComponent implements OnInit {

  constructor(private ProgrammerService: ProgrammerService, private formBuilder: FormBuilder) { }

  PhotoFileName:string="anonymus.png";
  PhotoFilePath:string='';
  prog!:Programmer;
  genderList = ["Male","Female"];
  Programmers: Programmer[] = [];
  Programmer!: Programmer;
  
  addProgrammerForm = this.formBuilder.group({
    firstName:  [ '', [Validators.required, Validators.minLength(1) ]],
    lastName: [ '', [Validators.required, Validators.minLength(1)]] ,
    gender: [ '', [Validators.required, Validators.minLength(1)]] ,
    doj : [ '', [Validators.required ]],
    createdDate: [formatDate(new Date(), 'yyyy-MM-dd', 'en-US')],
    PhotoFileName: [this.ProgrammerService.PhotoUrl + this.PhotoFileName]
  });


  ngOnInit(): void {
    this.ProgrammerService.getProgrammers().subscribe((data) => {
        this.Programmers = data;
        console.log(data);
    });
  }

  addProgrammer(Programmer: Programmer): void {
    this.ProgrammerService.addProgrammer(Programmer).subscribe((data: any) => {
      console.log(data);
     
      this.ProgrammerService.getProgrammers().subscribe((data) => {
        this.Programmers = data;
        console.log(data);
    });
    },
     (error) => {console.log(error.error); }
    );
  }

  deleteProgrammer(ProgrammerId: number): void{
    this.ProgrammerService.deleteProgrammer(ProgrammerId)
    .subscribe((data) => {
      console.log(data);
      this.ProgrammerService.getProgrammers().subscribe((Programmers) => {
        this.Programmers = Programmers;
        console.log(data);
    });
    });
  }
 
  updateProgrammer(Programmer : Programmer){
    Programmer.programmerId = this.prog.programmerId;
    Programmer.createdDate = this.prog.createdDate;
    this.ProgrammerService.updateProgrammer(Programmer)
    .subscribe((data) => {
      console.log(data);
      this.ProgrammerService.getProgrammers().subscribe((Programmers) => {
        this.Programmers = Programmers;
        console.log(data);
    });
    });
  }

  uploadPhoto(event:any){
    var file=event.target.files[0];
    const formData:FormData=new FormData();
    formData.append('uploadedFile',file,file.name);

    this.ProgrammerService.UploadPhoto(formData).subscribe((data:any)=>{
      this.PhotoFileName=data.toString();
      this.PhotoFilePath=this.ProgrammerService.PhotoUrl+this.PhotoFileName;
    })
  }

  deleteClick(item:any){
    if(confirm('Are you sure??')){
      this.ProgrammerService.deleteProgrammer(item.programmerId).subscribe(data=>{
        console.log(data);
        this.ProgrammerService.getProgrammers().subscribe((Programmers) => {
          this.Programmers = Programmers;
          console.log(data);
      });
    });
    }
  }

  closeClick(){
    this.refreshProgrammerList();
  }

  editClick(item:any){
    this.prog=item;
  }
  refreshProgrammerList(){
    this.ProgrammerService.getProgrammers().subscribe(data=>{
      this.Programmers=data;
    });
  }

}
