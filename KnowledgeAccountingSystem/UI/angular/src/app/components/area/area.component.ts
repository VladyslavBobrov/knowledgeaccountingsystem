import { AuthService } from './../../services/auth.service';
import { AreaService } from 'src/app/services/area.service';
import { Component, NgZone, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Observable, ObservableInput } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { Area } from 'src/app/interfaces/area';
import { ChatService } from 'src/app/services/chat.service';
import { Message } from 'src/app/interfaces/Message';

@Component({
  selector: 'app-area',
  templateUrl: './area.component.html',
  styleUrls: ['./area.component.css']
})
export class AreaComponent implements OnInit {

  areaId!: number;
  area: Area = {name: '', description: '', id: 0};
  messages = new Array<Message>();
  userId!: number;
  text!: string;
  constructor(private route: ActivatedRoute,
              private authService: AuthService,
              private areaService: AreaService,
              private chatService: ChatService,
              private _ngZone: NgZone ) {
      this.subscribeToEvents();
    }

  ngOnInit() {
    this.areaId = Number(this.route.snapshot.paramMap.get('id'));
    this.userId = this.authService.getCurrentUserValue().userId;
    this.text = '';
    this.route.params.pipe(
      switchMap((params: Params) => this.areaService.getArea(params.id))
      ).subscribe((area: any) => {
        this.area = area;
        console.log(area);
        this.getAllMessages(area.id);
      });

  }
  getAllMessages(areaId: number){
    this.chatService.getAllMessages(areaId).subscribe(
      (messages: any) => {
        this.messages = messages.reverse();
      }
    );
  }

  addMessage(){
    this.chatService.addMessage({ text: this.text, userId: this.userId, date: new Date(), areaId : this.areaId})
    .subscribe(arg => { this.text = ''; });

  }
  getArea(): void{
    this.areaService.getArea(this.areaId).subscribe(
      (area: any) => {
        this.area = area;
      },
      (error) => {

      }
    );
  }

  private subscribeToEvents(): void {

    this.chatService.messageReceived.subscribe((message: Message) => {
      this._ngZone.run(() => {
        if (message.areaId === this.areaId) {
          this.messages.push(message);
        }
      });
    });

  }

}
