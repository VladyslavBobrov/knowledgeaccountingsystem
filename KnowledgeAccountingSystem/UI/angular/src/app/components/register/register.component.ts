import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserRegister } from './../../interfaces/user-register';
import { AuthService } from './../../services/auth.service';
import { FormBuilder, FormControl, FormGroup,Validators } from '@angular/forms';
import { Role } from 'src/app/constants/Role';
import { ProgrammerService } from 'src/app/services/programmer.service';
import { Programmer } from '../../interfaces/Programmer';
import {formatDate} from '@angular/common';
import { ProgCheck } from 'src/app/interfaces/ProgCheck';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(private formBuilder: FormBuilder, private authService: AuthService,private ProgrammerService: ProgrammerService, private router: Router) { }

  programmerId!: number;
  isIdReceived!: boolean;
  ProgCheck!: ProgCheck;
  checkError!: string;
  reigisterError!: string;

  PhotoFileName:string="anonymus.png";
  PhotoFilePath=this.ProgrammerService.PhotoUrl + this.PhotoFileName;
  genderList = ["Male","Female"];
  Programmer!: Programmer;
  
  roleList = ["Manager","Programmer"];
  form!: FormGroup;

  isAdded=false;
  addProgrammerForm = this.formBuilder.group({
    firstName!:  new FormControl('',[Validators.required, Validators.minLength(1)]),
    lastName!: new FormControl('',[Validators.required, Validators.minLength(1)]),
    gender!: new FormControl('',[Validators.required, Validators.minLength(1)]),
    doj!: new FormControl('',Validators.required),
    createdDate!: [formatDate(new Date(), 'yyyy-MM-dd', 'en-US')],
    PhotoFileName: [this.PhotoFileName]
  });

  addProgrammer(Programmer: Programmer): void {
    this.Programmer = Programmer;
    this.ProgrammerService.addProgrammer(Programmer).subscribe((data: any) => {
      console.log(data);
    },
     (error) => {console.log(error.error); }
    );
    this.isAdded = true;
  }
  uploadPhoto(event:any){
    var file=event.target.files[0];
    const formData:FormData=new FormData();
    formData.append('uploadedFile',file,file.name);

    this.ProgrammerService.UploadPhoto(formData).subscribe((data:any)=>{
      this.PhotoFileName=data.toString();
      this.PhotoFilePath=this.ProgrammerService.PhotoUrl+this.PhotoFileName;
    })
  }
  ngOnInit() {
    this.form = new FormGroup({
      login : new FormControl('',Validators.required),
      role : new FormControl('',Validators.required),
      password :new FormControl('',Validators.required),
      email! :new FormControl('',[Validators.required, Validators.email])
    })
    this.isIdReceived = false;
  }

  getProgrammerById(){
    this.authService.getProgrammerId(this.Programmer).subscribe(
      (id: any) => {
        this.programmerId = id;
        this.isIdReceived = true;
      },
      (exc) => {
        this.checkError = exc.error;
      }
    );
  }

  register(){
    this.getProgrammerById();
    alert(this.programmerId);
    let userRegister = this.form.value;
    userRegister['programmerId'] = this.programmerId;

    this.authService.register(userRegister).subscribe(
      ()=>{
        this.router.navigate(['/login']);
      },
      (exc) =>{
            this.reigisterError = exc.error;
      }
    )
  }


}
