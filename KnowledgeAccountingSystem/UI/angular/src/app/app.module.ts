import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { JwtInterseptor } from './helpers/jwt-inspector';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { NgxPaginationModule } from 'ngx-pagination';
import { IsSubscribeOnAreaPipe } from './pipes/is-subscribe-on-area.pipe';

import { ChartsModule } from 'ng2-charts';
import{HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http'
import{FormsModule,ReactiveFormsModule} from '@angular/forms';
import { AdminAreaComponent } from './components/admin-area/admin-area.component';
import { AreaComponent } from './components/area/area.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { ProgrammersComponent } from './components/programmers/programmers.component';
import { ProgrammerInformationComponent } from './components/programmer-information/programmer-information.component';
import { ProgrammerAreaComponent } from './components/programmer-area/programmer-area.component'
import { CommonModule } from '@angular/common';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { ManagerComponent } from './components/manager/manager.component';

@NgModule({
  declarations: [
    AppComponent,
    AdminAreaComponent,
    AreaComponent,
    LoginComponent,
    ProgrammersComponent,
    RegisterComponent,
    ProgrammerInformationComponent,
    ProgrammerAreaComponent,
    IsSubscribeOnAreaPipe,
    PageNotFoundComponent,
    ManagerComponent
  ],
  imports: [
    ReactiveFormsModule,
    NgxPaginationModule,
    FormsModule,
    BrowserModule,
    CommonModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule
  ],
  providers:[{
    provide: HTTP_INTERCEPTORS, useClass: JwtInterseptor, multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
