export interface ProgrammerArea {
    id: number;
    name: string;
    description: string;
    isSubscribed: boolean;
  }
  