export interface User {
    userId: number;
    programmerId: number;
    login: string;
    role: string;
    email: string;
    token: string;
  }
  