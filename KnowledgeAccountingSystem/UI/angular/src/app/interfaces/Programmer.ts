export interface Programmer {
    programmerId: number;
    firstName: string;
    lastName: string;
    gender: string;
    userId: number;  
    doj : Date;
    createdDate: Date;
    PhotoFileName: string;
  }
  