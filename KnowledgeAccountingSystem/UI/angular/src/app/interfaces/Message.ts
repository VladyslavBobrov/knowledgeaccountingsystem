export interface Message {
    id: number;
    areaId: number;
    userId: number;
    login: string;
    text: string;
    date: Date;
}
