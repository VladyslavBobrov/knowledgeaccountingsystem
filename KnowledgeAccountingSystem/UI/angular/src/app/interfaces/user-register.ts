export interface UserRegister {
    programmerId: number;
    login: string;
    password: string;
    role:string;
    email: string;
  }
  