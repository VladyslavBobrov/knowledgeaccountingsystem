import { Programmer } from './../interfaces/Programmer';
import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders,HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
  })
  export class ProgrammerService {
  
  getUrl = 'https://localhost:44355/api/Programmer';
  readonly PhotoUrl="https://localhost:44355/Photos/";

  constructor(private http: HttpClient) { }
  
    getProgrammers(): Observable<Programmer[]>{
      return this.http.get<Programmer[]>(this.getUrl);
    }
    getProgrammerId(id :number): Observable<Programmer[]>{
      return this.http.get<Programmer[]>(this.getUrl+'/'+id);
    }
    getProgrammerListForManager(): Observable<Programmer[]>{
      return this.http.get<Programmer[]>("https://localhost:44355/api/Programmer/ProgrammerListForManager");
    }
    addProgrammer( programmer: Programmer): Observable<any>{
      const headers1 = new HttpHeaders().set('Content-Type', 'application/json').set('Accept', 'application/json').set('Authorization', 'my_token');
      return this.http.post(this.getUrl, JSON.stringify(programmer), {headers:headers1}).pipe(catchError(this.handleError));
    }
  
    deleteProgrammer(programmerId: number) : Observable<any> {
      const headers1 = new HttpHeaders().set('Content-Type', 'application/json').set('Accept', 'application/json').set('Authorization', 'my_token');
        return this.http.delete(this.getUrl + '/' + programmerId,{headers:headers1}).pipe(catchError(this.handleError));
    }
    handleError(error: HttpErrorResponse) {
      return throwError(error);
    }
    updateProgrammer(val:any){
      const headers1 = new HttpHeaders().set('Content-Type', 'application/json').set('Accept', 'application/json').set('Authorization', 'my_token');
      return this.http.put(this.getUrl,val,{headers:headers1}).pipe(catchError(this.handleError));
    }
  
    UploadPhoto(val:any){
      const headers1 = new HttpHeaders().set('Content-Type', 'application/json').set('Accept', 'application/json').set('Authorization', 'my_token');
      return this.http.post(this.getUrl+'/SaveFile/',JSON.stringify(val),{headers:headers1}).pipe(catchError(this.handleError));
    }
  }
  