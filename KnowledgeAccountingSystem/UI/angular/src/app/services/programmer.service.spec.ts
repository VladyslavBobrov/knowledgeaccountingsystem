/* tslint:disable:no-unused-variable */
import { TestBed, async, inject } from '@angular/core/testing';
import { ProgrammerService } from './programmer.service';

describe('Service: ProgrammerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProgrammerService]
    });
  });

  it('should ...', inject([ProgrammerService], (service: ProgrammerService) => {
    expect(service).toBeTruthy();
  }));
});
