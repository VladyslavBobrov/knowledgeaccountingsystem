import { Observable, throwError } from 'rxjs';
import { AddArea } from './../interfaces/add-area';
import { HttpClient ,HttpHeaders,HttpErrorResponse} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Area } from '../interfaces/area';
import { ProgrammerArea } from '../interfaces/prog-area';
import { catchError } from 'rxjs/internal/operators/catchError';

@Injectable({
  providedIn: 'root'
})
export class AreaService {

constructor(private http: HttpClient) { }

getAllAreas(): Observable<Area[]>{
  return this.http.get<Area[]>('https://localhost:44355/api/Area');
}

addArea(addArea: AddArea){
  const headers1 = new HttpHeaders().set('Content-Type', 'application/json').set('Accept', 'application/json').set('Authorization', 'my_token');

  return this.http.post('https://localhost:44355/api/Area', JSON.stringify(addArea),{headers:headers1}).pipe(catchError(this.handleError));
}
deleteArea(areaId: number){
  const headers1 = new HttpHeaders().set('Content-Type', 'application/json').set('Accept', 'application/json').set('Authorization', 'my_token');

  return this.http.delete('https://localhost:44355/api/Area?AreaId=' + areaId,{headers:headers1}).pipe(catchError(this.handleError));
}

getProgrammerAreas(programmerId:any): Observable<ProgrammerArea[]>{
  return this.http.get<ProgrammerArea[]>('https://localhost:44355/api/Area/getProgrammerAreas?programmerId=' + programmerId);
}
getAreasByProgrammerId(programmerId:any): Observable<ProgrammerArea[]>{
  return this.http.get<ProgrammerArea[]>('https://localhost:44355/api/Area/getAreasByProgrammerId?programmerId=' + programmerId);
}

addProgrammerArea(programmerArea: any){
  const headers1 = new HttpHeaders().set('Content-Type', 'application/json').set('Accept', 'application/json').set('Authorization', 'my_token');
  return this.http.post('https://localhost:44355/api/Area/addProgrammerArea',  JSON.stringify(programmerArea),{headers:headers1}).pipe(catchError(this.handleError));
}
deleteProgrammerArea(programmerArea: any){
  const headers1 = new HttpHeaders().set('Content-Type', 'application/json').set('Accept', 'application/json').set('Authorization', 'my_token');
  return this.http.delete('https://localhost:44355/api/Area/deleteProgrammerArea?programmerId='+ programmerArea.programmerId+ '&areaId=' + programmerArea.id,{headers:headers1}).pipe(catchError(this.handleError));
}

getArea(areaId: number){
  return this.http.get('https://localhost:44355/api/Area/getArea?AreaId='+ areaId);
}
handleError(error: HttpErrorResponse) {
  return throwError(error);
}
}
