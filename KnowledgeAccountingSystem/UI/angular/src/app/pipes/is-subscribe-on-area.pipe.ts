import { Pipe, PipeTransform } from '@angular/core';
import { ProgrammerArea} from '../interfaces/prog-area';

@Pipe({
  name: 'IsSubscribeOnArea'
})
export class IsSubscribeOnAreaPipe implements PipeTransform {

  transform(items: ProgrammerArea[], IsSubscribed: boolean): any {
    if(!items || IsSubscribed === undefined){
      return items;
    }

    return items.filter(it => it.isSubscribed === IsSubscribed);
  }

}
