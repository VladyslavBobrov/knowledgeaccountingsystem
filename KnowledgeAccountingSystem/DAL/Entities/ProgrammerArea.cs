﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DAL.Entities
{
    public class ProgrammerArea
    {
        [Key]
        public long ProgrammerAreaId { get; set; }
    
        public long ProgrammerId { get; set; }
    
        public long AreaId { get; set; }


    }
}
