﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DAL.Entities
{
    public class Programmer
    {
        [Key]
        public long ProgrammerId { get; set; }


        [ForeignKey("UserId")]
        public long? UserId { get; set; }
        public User User { get; set; }

        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        public string Gender { get; set; }
        public DateTime DOJ { get; set; }

        public DateTime CreatedDate { get; set; }

        public string PhotoFileName { get; set; }

    }
}
