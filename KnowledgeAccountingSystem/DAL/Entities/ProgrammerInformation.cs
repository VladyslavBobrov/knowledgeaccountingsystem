﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DAL.Entities
{
    public class ProgrammerInformation
    {
        [System.ComponentModel.DataAnnotations.Key]
        public long InformationId { get; set; }

        public string Information { get; set; }

        public string Knowledge { get; set; }
        [ForeignKey("ProgrammerId")]
        public long ProgrammerId { get; set; }

        public Programmer Programmer { get; set; }

    }

}
