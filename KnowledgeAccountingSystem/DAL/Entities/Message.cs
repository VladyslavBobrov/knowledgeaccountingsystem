﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Entities
{
    public class Message
    {
        public long Id { get; set; }

        public long UserId { get; set; }

        public User User { get; set; }

        public long AreaId { get; set; }

        public Area Area { get; set; }

        public string Text { get; set; }

        public DateTime Date { get; set; }
    }

}
