﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Entities
{
    public class User
    {
        public long Id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string Role { get; set; }
        public Programmer Programmer { get; set; }

        public List<Message> Messages { get; set; }

        public User()
        {
            Messages = new List<Message>();
        }


    }

}
