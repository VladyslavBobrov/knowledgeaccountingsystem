﻿using DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class Repository<T> : IRepository<T> where T : class
    {
        private readonly KASDbContext db;
        public Repository(KASDbContext context)
        {
            db = context; 
        }
        public Task AddAsync(T entity)
        {
            db.Set<T>().Add(entity);
            return db.SaveChangesAsync();
        }

        public void Delete(T entity)
        {
            db.Set<T>().Remove(entity);
        }

        public Task DeleteByIdAsync(int id)
        {
            db.Set<T>().Remove(db.Set<T>().Find(id));
            return db.SaveChangesAsync();
        }

        public IEnumerable<T> FindAll()
        {
            return db.Set<T>().ToList();
        }
        public async Task<List<T>> GetAllAsync()
        {
            return await db.Set<T>().ToListAsync();
        }

        public async Task<List<T>> GetAllByAsync(Expression<Func<T, bool>> expression)
        {
            return await db.Set<T>().Where(expression).ToListAsync();

        }

        public async Task<List<T>> GetAllByAsync(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includes)
        {
            var query = db.Set<T>().Where(predicate);
            return await includes.Aggregate(query, (current, includeProperty) => current.Include(includeProperty)).ToListAsync();
        }


        public async Task<T> GetByAsync(Expression<Func<T, bool>> expression)
        {
            return await db.Set<T>().FirstOrDefaultAsync(expression);
        }
        public async Task DeleteAsync(Expression<Func<T, bool>> expression)
        {
            var item = await db.Set<T>().FirstOrDefaultAsync(expression);
            if (item != null)
            {
                db.Set<T>().Remove(item);
                await db.SaveChangesAsync();
            }
        }

        public async Task Update(T entity)
        {
            db.Entry(entity).State = EntityState.Modified;
            await db.SaveChangesAsync();
        }
    }
}
