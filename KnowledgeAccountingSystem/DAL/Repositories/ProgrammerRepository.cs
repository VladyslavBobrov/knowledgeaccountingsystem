﻿using DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class ProgrammerRepository : Repository<Programmer>,IProgrammerRepository
    {
        private readonly KASDbContext db;
        public ProgrammerRepository(KASDbContext context) : base(context)
        {
            db = context;
        }

        public async Task<Programmer> GetByIdAsync(int id)
        {
            return await db.Programmers.FirstAsync(i => i.ProgrammerId == id);
        }


    }
}
