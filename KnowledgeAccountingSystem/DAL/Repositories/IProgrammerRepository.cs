﻿using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public interface IProgrammerRepository : IRepository<Programmer>
    {
        Task<Programmer> GetByIdAsync(int id);
    }
}
