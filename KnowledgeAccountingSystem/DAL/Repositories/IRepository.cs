﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public interface IRepository<TEntity>
    {
        IEnumerable<TEntity> FindAll();

      
        Task AddAsync(TEntity entity);
        Task<List<TEntity>> GetAllAsync();
        Task Update(TEntity entity);

        void Delete(TEntity entity);
        Task DeleteAsync(Expression<Func<TEntity, bool>> expression);
        Task DeleteByIdAsync(int id);

        Task<List<TEntity>> GetAllByAsync(Expression<Func<TEntity, bool>> expression);
        Task<TEntity> GetByAsync(Expression<Func<TEntity, bool>> expression);
        Task<List<TEntity>> GetAllByAsync(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includes);

    }
}
