﻿using DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL
{
    public class KASDbContext : DbContext
    {
        public KASDbContext(DbContextOptions<KASDbContext> options) : base(options)
        {
            Database.EnsureCreated();
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // использование Fluent API

            modelBuilder
                .Entity<User>()
                .HasOne(u => u.Programmer)
                .WithOne(p => p.User)
                .HasForeignKey<Programmer>(p => p.UserId);

            modelBuilder.Entity<ProgrammerArea>()
            .HasKey(t => new { t.ProgrammerAreaId });

        }

        public DbSet<Programmer> Programmers { get; set; }

        public DbSet<ProgrammerInformation> ProgrammerInformations { get; set; }

        public DbSet<User> Users { get; set; }

        public DbSet<Area> Areas { get; set; }

        public DbSet<ProgrammerArea> ProgrammerAreas { get; set; }

        public DbSet<Message> Messages { get; set; }

    }
}
